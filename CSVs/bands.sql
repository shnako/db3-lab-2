/* Note that at this stage you only need to worry about finding the key */
/* of each relation, representing it, as well as adding the foreign    */
/* key and NOT NULL constraints. Other more sophisticated constraints */
/* are not necessary for the purposes of this exercise.              */


CREATE TABLE  Band (
       bid INTEGER  PRIMARY KEY,
       name  VARCHAR(50) NOT NULL,
       country  VARCHAR(20),
       webpage   VARCHAR(120)
);

CREATE TABLE Release (


    ==== Complete ====

);

CREATE TABLE Song (


    ==== COMPLETE ====

);

CREATE TABLE Member (
     
     ==== COMPLETE ====


);

CREATE TABLE MemberOf (

      ==== COMPLETE ====

);

