1.
SELECT Band.name AS Band_Name, title AS Song_Title
FROM Band NATURAL JOIN (SELECT Release.bid, Song.title
		FROM Release JOIN Song ON Release.rid = Song.rid
		GROUP BY Song.title, Release.bid
		HAVING COUNT(Release.rid) > 1) AS T1
ORDER BY Song_Title

2.
CREATE VIEW view_band_release_song AS
SELECT Band.name as Band_Name, COUNT(Song.cdbonus) AS Bonus_Num, Song.cdbonus
	FROM Band NATURAL JOIN Release JOIN Song USING (rid)
	GROUP BY Band.name, Song.cdbonus;
	
SELECT Band_Name, Bonus_Num
FROM 	(SELECT Band_Name, Bonus_Num
	FROM view_band_release_song
	WHERE cdbonus = 'Y'
	GROUP BY Band_name, Bonus_Num) T1
	UNION ALL
	(SELECT Band.name as Band_Name, 0 AS Bonus_Num
	FROM Band
	WHERE Band.name NOT IN (SELECT Band_Name
				FROM view_band_release_song
				WHERE cdbonus = 'Y'))
ORDER BY Band_Name

3.
SELECT Guitarist_Name, Band_Name, Start_Year, End_Year
FROM ( 
	(SELECT Member.name AS Guitarist_Name, Band.name AS Band_Name, MemberOf.startyear AS Start_Year, MemberOf.endyear AS End_Year
	FROM (Band JOIN MemberOf ON (Band.bid = MemberOf.bid)) NATURAL JOIN Member
	WHERE MemberOf.startyear IS NOT NULL AND
		MemberOf.instrument = 'guitar'
) UNION ALL (

SELECT Member.name AS Guitarist_Name, Band.name AS Band_Name, MIN(Release.year) AS Start_Year, MemberOf.endyear AS End_Year
	FROM (Release JOIN Band ON (Release.bid = Band.bid)) JOIN (MemberOf NATURAL JOIN Member) ON (Band.bid = MemberOf.bid)
	WHERE MemberOf.startyear IS NULL AND
		MemberOf.instrument = 'guitar'
	GROUP BY Member.name, Band.name, MemberOf.endyear
)
) AS Unq
ORDER BY Guitarist_Name, Start_Year

4.
SELECT Band.name AS Band_Name, Keyboard_Player
FROM Band LEFT JOIN (
	SELECT Band.bid, Member.name AS Keyboard_Player
	FROM Band JOIN (Member NATURAL JOIN MemberOf) ON (Band.bid = MemberOf.bid)
	WHERE instrument = 'keyboards'
) AS K ON K.bid = Band.bid
ORDER BY Band_Name, Keyboard_Player

5.
CREATE VIEW view_song_release AS
	SELECT Release.rid, Release.bid, Release.title AS Release_Title, Song.title AS Song_Title, Release.year, Release.type, Release.rating, Song.cdbonus
	FROM Song JOIN Release USING (rid);
	
	 
SELECT Band.name AS Band_Name, Member.name AS Singer_Name, S.Release_Title AS Album_Name, S.count AS Song_Num
FROM Band JOIN MemberOf ON (Band.bid = MemberOf.bid) JOIN Member ON (MemberOf.mid = Member.mid) JOIN
	(SELECT bid, Release_Title, year, Count(rid)
	FROM view_song_release
	GROUP BY Release_Title, bid, year
	HAVING COUNT(rid) = (SELECT COUNT(rid) as Song_Num
		FROM view_song_release
		WHERE type = 'album'
		GROUP BY rid
		ORDER BY Song_Num DESC
		LIMIT 1)) S ON (S.bid = Band.bid)
WHERE MemberOf.instrument = 'vocals' AND
	(startyear IS NULL AND endyear IS NULL) OR
	(startyear IS NULL AND endyear >= S.year) OR
	(startyear <= S.year AND endyear IS NULL) OR
	(startyear <= S.year AND endyear >= S.year)
ORDER BY Band.name
