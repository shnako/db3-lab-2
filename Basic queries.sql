1.
SELECT * 
FROM Member 
WHERE name LIKE 'Tim %';

2.
SELECT * 
FROM Member 
WHERE name LIKE 'Tim %'
UNION
SELECT * 
FROM Member 
WHERE name LIKE 'Tom %';

3.
SELECT * 
FROM Member 
WHERE name LIKE 'Tim %' OR name LIKE 'Tom %';

4.
CREATE INDEX idx_member_name ON Member USING gin(name gin_trgm_ops);

This does not help in our case. The 3 costs incurred are likely larger than the benefits.

5.
CREATE INDEX idx_band_name ON Band (name);
SELECT Member.name
FROM Band, Member, MemberOf
WHERE Band.name = 'Iron Maiden'
	AND Band.bid = MemberOf.bid
	AND Member.mid = MemberOf.mid
	AND MemberOf.endyear IS NULL

6.
SELECT Band.name AS Bandname, COUNT(*) AS Number_Of_Members
FROM Band NATURAL JOIN MemberOf
GROUP BY Band.bid
ORDER BY Number_Of_Members

7.
CREATE VIEW band_members AS
SELECT Member.name AS membername, Band.name AS bandname
FROM Band, Member, MemberOf
WHERE Band.bid = MemberOf.bid AND Member.mid = MemberOf.mid

8.
SELECT bandname, COUNT(DISTINCT membername) AS member_count
FROM band_members
GROUP BY bandname
ORDER BY member_count

9.
SELECT *
FROM Band NATURAL JOIN Member

Indices should be added to both tables for the name column.

10. 
SELECT COUNT(*)
FROM Release
WHERE rating < 5 OR rating >= 5;

Different because of the NULL values on rating.

11.
SELECT COUNT(DISTINCT mid) AS Bass_Player_Count
FROM MemberOf
WHERE instrument = 'bass'

12.
SELECT Member.name AS Drummer_Name, Release.title AS Release_Name, Release.year AS Year
FROM Band, Release, MemberOf, Member
WHERE MemberOf.instrument = 'drums' AND
	Band.bid = Release.bid AND
	Release.bid = MemberOf.bid AND
	MemberOf.mid = Member.mid AND
	(
		(startyear IS NULL AND endyear IS NULL) OR
		(startyear IS NULL AND endyear >= Year) OR
		(startyear <= Year AND endyear IS NULL) OR
		(startyear <= Year AND endyear >= Year)
	)
ORDER BY Drummer_Name, Year

We also check that the member was actually a member of that band when the release occurred.
