1.
SELECT Band.name AS Band_Name, Song.title AS Song_Title
FROM Song JOIN (Band NATURAL JOIN Release) ON (Release.rid = Song.rid)
GROUP BY Band.name, Song.title
HAVING COUNT(Release.rid) > 1
ORDER BY Band_Name

Result: 1728 rows

2.
SELECT Band_Name, Bonus_Num
FROM 	(SELECT Band.name as Band_Name, COUNT(Song.cdbonus) AS Bonus_Num
	FROM Band NATURAL JOIN Release JOIN Song USING (rid)
	WHERE Song.cdbonus = 'Y'
	GROUP BY Band.name) T1
	UNION
	(SELECT Band.name as Band_Name, 0 AS Bonus_Num
	FROM Band NATURAL JOIN Release JOIN Song USING (rid)
	WHERE Band.name NOT IN (SELECT DISTINCT Band.name as Band_Name
				FROM Band NATURAL JOIN Release JOIN Song USING (rid)
				WHERE Song.cdbonus = 'Y'
				GROUP BY Band.bid
				ORDER BY Band.name))
ORDER BY Band_Name

Result: 11 rows - 6/1/1/3/1/1/1/1/1/2/6

3. 
SELECT Guitarist_Name, Band_Name, Start_Year, End_Year
FROM ( 
	(SELECT Member.name AS Guitarist_Name, Band.name AS Band_Name, MemberOf.startyear AS Start_Year, MemberOf.endyear AS End_Year
	FROM Band JOIN (MemberOf NATURAL JOIN Member) ON (Band.bid = MemberOf.bid)
	WHERE MemberOf.startyear IS NOT NULL AND
		MemberOf.instrument = 'guitar'
) UNION (

SELECT Member.name AS Guitarist_Name, Band.name AS Band_Name, MIN(Release.year) AS Start_Year, MemberOf.endyear AS End_Year
	FROM Release JOIN (Band JOIN (MemberOf NATURAL JOIN Member) ON (Band.bid = MemberOf.bid)) ON (Release.bid = Band.bid)
	WHERE MemberOf.startyear IS NULL AND
		MemberOf.instrument = 'guitar'
	GROUP BY Member.name, Band.name, MemberOf.endyear
)
) AS Unq
	ORDER BY Guitarist_Name, Start_Year

4.
SELECT Band.name AS Band_Name, Keyboard_Player
FROM Band NATURAL JOIN 
	(SELECT Band.bid, MMO.name AS Keyboard_Player
	FROM Band JOIN (MemberOf JOIN Member ON (Member.mid = MemberOf.mid AND MemberOf.instrument = 'keyboards')) AS MMO ON (Band.bid = MMO.bid)
	WHERE MMO.instrument = 'keyboards'

	UNION

	SELECT Band.bid, NULL AS Keyboard_Player
	FROM Band

	EXCEPT

	SELECT Band.bid, NULL AS Keyboard_Player
	FROM Band
	WHERE Band.bid IN (SELECT Band.bid AS Band_Name
				FROM Band JOIN (MemberOf JOIN Member ON (Member.mid = MemberOf.mid AND MemberOf.instrument = 'keyboards')) AS MMO ON (Band.bid = MMO.bid)
				WHERE MMO.instrument = 'keyboards')) AS United

ORDER BY Band_Name, Keyboard_Player

5.
SELECT DISTINCT Band.name AS Band_Name, Member.name AS Singer_Name, Release.title AS Album_Name, Song_Num
FROM ((Band NATURAL JOIN Release) JOIN Song ON Release.rid = Song.rid) JOIN (Member NATURAL JOIN MemberOf) ON Band.bid = MemberOf.bid
JOIN (
	SELECT Release.rid, COUNT(*) AS Song_Num
		FROM Song, Release
		WHERE Song.rid = Release.rid
		GROUP BY Release.rid
		HAVING COUNT(*) = (
			SELECT COUNT(*) AS song_count
			FROM Song, Release
			WHERE Song.rid = Release.rid AND
				Release.type = 'album'
			GROUP BY Release.rid
			ORDER BY song_count DESC
			LIMIT 1
	) 
)AS RS ON Release.rid = RS.rid
WHERE (startyear IS NULL AND endyear IS NULL) OR
	(startyear IS NULL AND endyear >= Release.year) OR
	(startyear <= Release.year AND endyear IS NULL) OR
	(startyear <= Release.year AND endyear >= Release.year)
ORDER BY Band_Name
