1.
SELECT DISTINCT Band.name AS Band_Name, s1.title AS Song_title
FROM Band, Release AS r1, Release AS r2, Song AS s1, Song AS s2
WHERE 	    Band.bid = r1.bid
	AND Band.bid = r2.bid
	AND r1.rid = s1.rid
	AND r2.rid = s2.rid
	AND s1.title = s2.title
	AND s1.rid <> s2.rid
ORDER BY Band.name

2.
SELECT *
FROM
	(SELECT Band.name as Band_Name, COUNT(Song.cdbonus) AS Bonus_Num
	FROM Song JOIN (Band NATURAL JOIN Release) USING (rid)
	WHERE Song.cdbonus = 'Y'
	GROUP BY  Band.bid) AS T1
	UNION
	(SELECT Band.name as Band_Name, 0 AS Bonus_Num
	FROM  Band NATURAL JOIN Song JOIN Release USING (rid)
	GROUP BY  Band.bid)
	EXCEPT
	(SELECT Band.name as Band_Name, 0 AS Bonus_Num
	FROM Song JOIN (Band NATURAL JOIN Release) USING (rid)
	WHERE Song.cdbonus = 'Y'
	GROUP BY  Band.bid)
ORDER BY Band_Name

3.
SELECT Member.name AS Member_Name, Band.name as Band_Name, Start_Year, endyear AS End_Year
FROM 
	(SELECT Member.mid, Band.bid, MemberOf.startyear AS Start_Year, MemberOf.endyear
	FROM Band JOIN (MemberOf NATURAL JOIN Member) ON (Band.bid = MemberOf.bid)
	WHERE MemberOf.instrument = 'guitar' AND MemberOf.startyear IS NOT NULL
	UNION
	SELECT Member.mid, Band.bid, MIN(Release.year) AS Start_Year, MemberOf.endYear
	FROM Release JOIN (Band JOIN (MemberOf NATURAL JOIN Member) ON (Band.bid = MemberOf.bid)) ON (Release.bid = Band.bid)
	WHERE MemberOf.instrument = 'guitar' AND MemberOf.startyear IS NULL
	GROUP BY Member.mid, Band.bid, MemberOf.endYear) T1 NATURAL JOIN Band JOIN Member ON (T1.mid = Member.mid)
ORDER BY Member_Name, Band_Name

4.
SELECT Band_Name, Keyboard_Player
FROM (SELECT Band.name AS Band_Name, MMO.name AS Keyboard_Player
	FROM Band JOIN (MemberOf JOIN Member ON (Member.mid = MemberOf.mid AND MemberOf.instrument = 'keyboards')) AS MMO ON (Band.bid = MMO.bid)
	WHERE MMO.instrument = 'keyboards'

	UNION

	SELECT Band.name AS Band_Name, NULL AS Keyboard_Player
	FROM Band
	WHERE Band.bid NOT IN (SELECT Band.bid AS Band_Name
				FROM Band JOIN (MemberOf JOIN Member ON (Member.mid = MemberOf.mid AND MemberOf.instrument = 'keyboards')) AS MMO ON (Band.bid = MMO.bid)
				WHERE MMO.instrument = 'keyboards')) AS United
 
ORDER BY Band_Name, Keyboard_Player

5.
SELECT Band.name AS Band_Name, Member.name AS Singer_Name, S.title AS Album_Name, S.count AS Song_Num
FROM Band JOIN MemberOf ON (Band.bid = MemberOf.bid) JOIN Member ON (MemberOf.mid = Member.mid) JOIN
	(SELECT Release.bid, Release.title, Release.year, Count(*)
	FROM Release JOIN Song ON (Release.rid = Song.rid)
	GROUP BY Release.title, Release.bid, Release.year
	HAVING COUNT(*) = (SELECT COUNT(*) as Song_Num
		FROM Release JOIN Song ON (Release.rid = Song.rid)
		WHERE Release.type = 'album'
		GROUP BY Release.title, Release.bid, Release.rid
		ORDER BY Song_Num DESC
		LIMIT 1)) S ON (S.bid = Band.bid)
WHERE MemberOf.instrument = 'vocals' AND
	(startyear IS NULL AND endyear IS NULL) OR
	(startyear IS NULL AND endyear >= S.year) OR
	(startyear <= S.year AND endyear IS NULL) OR
	(startyear <= S.year AND endyear >= S.year)
ORDER BY Band.name
