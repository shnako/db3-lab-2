CREATE TABLE Band (
	bid INTEGER PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	country VARCHAR(20),
	webpage VARCHAR(120)
);

CREATE TABLE Release (
	rid INTEGER PRIMARY KEY,
	bid INTEGER REFERENCES Band(bid),
	title VARCHAR(120) NOT NULL,
	year INTEGER,
	type VARCHAR(10),
	rating INTEGER
);

CREATE TABLE Song (
	title VARCHAR(120),
	rid INTEGER REFERENCES Release(rid),
	cdbonus CHAR(1),
	PRIMARY KEY(title, rid)
);

CREATE TABLE Member (
	mid INTEGER PRIMARY KEY,
	name VARCHAR(50),
	stillalive CHAR(1)
);

CREATE TABLE MemberOf (
	mid INTEGER REFERENCES Member(mid),
	bid INTEGER REFERENCES Band(bid),
	startyear INTEGER,
	endyear INTEGER,
	instrument VARCHAR(15),
	UNIQUE(mid, bid, startyear)
);

CREATE VIEW view_song_release AS
	SELECT Release.rid, Release.bid, Release.title AS Release_Title, Song.title AS Song_Title, Release.year, Release.type, Release.rating, Song.cdbonus
	FROM Song JOIN Release USING (rid);
